<?php
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var string $login */
UtilisateurRepository::supprimerParLogin($login);
echo "<h3>L'utilisateur de login:"; echo htmlspecialchars($login); echo "a bien été supprimé.</h3>";
require __DIR__ . '/liste.php';
?>
