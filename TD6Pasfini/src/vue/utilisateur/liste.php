<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var Utilisateur $utilisateurs */
foreach ($utilisateurs as $utilisateur){
    echo '<p> Utilisateur de login ';
    echo htmlspecialchars($utilisateur->getLogin());
    echo "\n";
    $login = $utilisateur->getLogin();
    $loginURL= rawurlencode($login);
    echo "<a href='?action=afficherDetail&login=$loginURL'>Détails</a>";
    echo "\n";
    echo "<a href='?action=afficherFormulaireMiseAJour&login=$loginURL'>Mettre à jour</a>";
    echo "\n";
    echo "<a href='?action=utilisateurSupprime&login=$loginURL'>Supprimer</a>";
    echo '</p>';
    }
echo "<a href='?action=afficherFormulaireCreation'>Ajouter un utilisateur</a>";
?>
</body>
</html>
