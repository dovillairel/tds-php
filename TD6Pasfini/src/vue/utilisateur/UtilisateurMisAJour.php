<?php
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;

$login = rawurldecode($_POST['login']);
$nom = $_POST['nom'];
$prenom = $_POST['prenom'];

$utilisateur = new Utilisateur($login, $nom, $prenom);
UtilisateurRepository::mettreAJour($utilisateur);
echo "<h3>L'utilisateur de login:"; echo htmlspecialchars($utilisateur->getLogin()); echo "a bien été mis à jour.</h3>";
require __DIR__ . '/liste.php';
?>
