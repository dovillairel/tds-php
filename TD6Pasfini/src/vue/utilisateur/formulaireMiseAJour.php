<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title>Formulaire Mise à Jour Utilisateur</title>
</head>

<body>

<form method="post" action="?action=mettreAJour">
    <fieldset>
        <legend>Mettre à jour l'utilisateur :</legend>

        <p>
            <label for="login_id">Login</label> :
            <input type="text" name="login" id="login_id" value="<?= /** @var Utilisateur $utilisateur */htmlspecialchars($utilisateur->getLogin()) ?>" readonly />
        </p>

        <p>
            <label for="nom_id">Nom</label> :
            <input type="text" name="nom" id="nom_id" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" required />
        </p>

        <p>
            <label for="prenom_id">Prénom</label> :
            <input type="text" name="prenom" id="prenom_id" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" required />
        </p>

        <!-- Champ caché pour spécifier l'action -->
        <input type="hidden" name="action" value="mettreAJour">

        <p>
            <input type="submit" value="Mettre à jour" />
        </p>
    </fieldset>
</form>

</body>
</html>
