<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\Utilisateur as ModeleUtilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = UtilisateurRepository::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('utilisateur/vueGenerale.php', ['utilisateurs' => $utilisateurs,"titre" => "Liste des utilisateurs", "cheminCorpsVue" => "/liste.php"]);
    }

    public static function afficherErreur(string $messageErreur) : void {
        self::afficherVue('utilisateur/vueGenerale.php', ['messageErreur' => $messageErreur, "cheminCorpsVue" => "/erreur.php"]);
    }

    public static function afficherFormulaireCreation(){
    self::afficherVue('utilisateur/vueGenerale.php', ["cheminCorpsVue" => "/formulaireCreation.php"]);
    }

    public static function creerUtilisateur(){
        self::afficherVue('utilisateur/creerUtilisateur.php');
    }

    public static function utilisateurSupprime()
    {
        $utilisateurs = UtilisateurRepository::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        $login = $_GET['login'];
        self::afficherVue('utilisateur/vueGenerale.php', ['login' => $login, 'utilisateurs' => $utilisateurs, "cheminCorpsVue" => "/utilisateurSupprime.php"]);
    }

    public static function afficherFormulaireMiseAJour()
    {
        $login = $_GET['login'];
        $utilisateur = UtilisateurRepository::recupererUtilisateurParLogin($login);
        self::afficherVue('utilisateur/vueGenerale.php', ["cheminCorpsVue" => "/formulaireMiseAJour.php", 'utilisateur' => $utilisateur]);

    }

    public static function mettreAJour(){
        $utilisateurs = UtilisateurRepository::recupererUtilisateurs(); //appel au modèle pour gérer la BD
    self::afficherVue('utilisateur/vueGenerale.php', ["cheminCorpsVue" => "/UtilisateurMisAJour.php", 'utilisateurs' => $utilisateurs]);
    }



    public static function afficherDetail() {
        // Récupération du login passé dans l'URL
        $login = isset($_GET['login']) ? $_GET['login'] : null;

        if ($login) {
            // Récupérer l'utilisateur par son login
            $utilisateur = UtilisateurRepository::recupererUtilisateurParLogin($login);

            if ($utilisateur) {
                // Si l'utilisateur existe, on affiche la vue détail
                self::afficherVue('utilisateur/detail.php', ['utilisateur' => $utilisateur]);
            } else {
                // Si l'utilisateur n'est pas trouvé, on affiche la vue d'erreur
                self::afficherErreur("Login inexistant");
            }
        } else {
            // Si aucun login n'est fourni, on affiche également la vue d'erreur
            self::afficherErreur("Login inexistant");
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }


}
?>
