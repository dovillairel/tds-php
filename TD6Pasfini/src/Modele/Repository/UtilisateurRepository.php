<?php

namespace App\Covoiturage\Modele\Repository;


use App\Covoiturage\Modele\DataObject\Utilisateur;

class UtilisateurRepository
{

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur(
            $utilisateurFormatTableau['loginBaseDeDonnees'],
            $utilisateurFormatTableau['prenomBaseDeDonnees'],
            $utilisateurFormatTableau['nomBaseDeDonnees']
        );
    }

    public static function recupererUtilisateurs() : array {
        $pdo = ConnexionBaseDeDonnees::getPdo();

        $requete = "SELECT * FROM utilisateur";
        $pdoStatement = $pdo->query($requete);

        $utilisateurs = [];
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    public function afficher() {
        echo htmlspecialchars($this->__toString());
    }

    public static function recupererUtilisateurParLogin(string $login): ?Utilisateur {
        $sql = "SELECT * FROM utilisateur WHERE loginBaseDeDonnees = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
        );

        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        $utilisateurFormatTableau = $pdoStatement->fetch();

        // Si aucun résultat, retourner null
        if ($utilisateurFormatTableau === false) {
            return null;
        }

        // Sinon, on construit l'objet Utilisateur
        return UtilisateurRepository::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public static function ajouter(Utilisateur $utilisateur){
        $login = $utilisateur->getLogin();
        $prenom = $utilisateur->getPrenom();
        $nom = $utilisateur->getNom();

        $sql = "INSERT INTO utilisateur (loginBaseDeDonnees,nomBaseDeDonnees,prenomBaseDeDonnees) VALUES (:value1, :value2, :value3)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            ":value1" => $login,
            ":value2" => $prenom,
            ":value3" => $nom,
        );

        $pdoStatement->execute($values);

        echo '<br>Ajout de l\'utilisateur :';
        echo htmlspecialchars($utilisateur->getLogin());
        echo 'effectué avec succès.<br>';
    }

    public static function supprimerParLogin($login){
        $sql = "DELETE FROM utilisateur WHERE loginBaseDeDonnees = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            ":loginTag" => $login,
        );
        $pdoStatement->execute($values);
        echo '<br>Suppression de l\'utilisateur :';
        echo htmlspecialchars($login);
        echo 'effectué avec succès.<br>';

    }

    public static function mettreAJour(Utilisateur $utilisateur){
    $sql = "UPDATE utilisateur SET nomBaseDeDonnees = :nomTag, prenomBaseDeDonnees = :prenomTag WHERE loginBaseDeDonnees = :loginTag";
    $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
    $values = array(
        ":nomTag" => $utilisateur->getPrenom(),
        ":prenomTag" => $utilisateur->getNom(),
        ":loginTag" => $utilisateur->getLogin(),
    );
    $pdoStatement->execute($values);
    }


}