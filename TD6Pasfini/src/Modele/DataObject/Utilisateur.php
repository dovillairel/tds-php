<?php
namespace App\Covoiturage\Modele\DataObject;
use App\Covoiturage\Modele\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;

class Utilisateur {

    // Déclaration de type pour les attributs
    private string $login;
    private string $nom;
    private string $prenom;

    public function __construct(string $login, string $nom, string $prenom) {
        // Tronquer le login à 64 caractères
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public function getLogin(): string {
        return $this->login;
    }

    public function setLogin(string $login): void {
        // Tronquer le login à 64 caractères
        $this->login = substr($login, 0, 64);
    }

    public function getNom(): string {
        return $this->nom;
    }

    public function setNom(string $nom): void {
        $this->nom = $nom;
    }

    public function getPrenom(): string {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): void {
        $this->prenom = $prenom;
    }


    public function __toString(): string {
        return "Utilisateur: $this->prenom $this->nom (Login: $this->login)";
    }

    public function afficher() {
        echo htmlspecialchars($this->__toString());
    }





}
?>


<?php