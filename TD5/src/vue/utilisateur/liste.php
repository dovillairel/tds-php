<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur[] $utilisateurs */
foreach ($utilisateurs as $utilisateur){
    echo '<p> Utilisateur de login ';
    echo htmlspecialchars($utilisateur->getLogin());
    echo '</p>';
    $login = $utilisateur->getLogin();
    $loginURL= rawurlencode($login);
    echo "<a href='?action=afficherDetail&login=$loginURL'>Detail utilisateur</a>";
    }
echo "<a href='?action=afficherFormulaireCreation'>Ajouter un utilisateur</a>";
?>
</body>
</html>
