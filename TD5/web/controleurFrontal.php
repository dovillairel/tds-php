<?php
require_once __DIR__ . '/../src/Controleur/ControleurUtilisateur.php';
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

/**
// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(true);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');
 * **/
$loader = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$loader ->register();
$loader->addNamespace('App\Covoiturage', __DIR__ . '/../src');




use App\Covoiturage\Controleur\ControleurUtilisateur;

$action = isset($_GET['action']) ? $_GET['action'] : null;

// Vérifier que l'action existe bien et que la méthode correspondante existe dans le contrôleur
if ($action && method_exists(ControleurUtilisateur::class, $action)) {
    // Appel de la méthode statique $action de ControleurUtilisateur
    ControleurUtilisateur::$action();
}else {
    // Si l'action n'est pas valide ou non spécifiée, on peut afficher une erreur ou rediriger
    echo "Action invalide ou non spécifiée.";
}

?>
