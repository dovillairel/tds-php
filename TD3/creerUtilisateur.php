<?php
require_once "utilisateurs.php";
require_once "ConnexionBaseDeDonnees.php";


$login = $_POST['login'] ?? 'Non spécifié';
$nom = $_POST['nom'] ?? 'Non spécifié';
$prenom = $_POST['prenom'] ?? 'Non spécifié';



echo "<h3>Informations de l'utilisateur :</h3>";

echo "Login: " . htmlspecialchars($login) . "<br>";
echo "Nom: " . htmlspecialchars($nom) . "<br>";
echo "Prénom: " . htmlspecialchars($prenom) . "<br>";

$utilisateur = new Utilisateur($login, $nom, $prenom);
$utilisateur->ajouter();
?>