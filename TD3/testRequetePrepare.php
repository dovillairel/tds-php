<?php
require_once "ConnexionBaseDeDonnees.php";
require_once "utilisateurs.php";

// Création d'un nouvel utilisateur
$nouvelUtilisateur = new Utilisateur("dupontj", "Dupont", "Jean");

// Ajout de l'utilisateur dans la base de données
//$nouvelUtilisateur->ajouter();

// Vérification : récupération de l'utilisateur à partir de la base de données
$utilisateurRecupere = Utilisateur::recupererUtilisateurParLogin("dupontjj");

if ($utilisateurRecupere === null) {
    echo "Erreur : l'utilisateur n'a pas pu être ajouté.";
} else {
    echo "L'utilisateur suivant a été ajouté et récupéré : " . $utilisateurRecupere;
}

?>