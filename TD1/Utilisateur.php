<?php
class Utilisateur {

    // Déclaration des types pour les attributs de classe
    private string $login;
    private string $nom;
    private string $prenom;

    // Getter pour nom
    public function getNom() : string {
        return $this->nom;
    }

    // Setter pour nom
    public function setNom(string $nom) : void {
        $this->nom = $nom;
    }

    // Getter pour prenom
    public function getPrenom() : string {
        return $this->prenom;
    }

    // Setter pour prenom
    public function setPrenom(string $prenom) : void {
        $this->prenom = $prenom;
    }

    // Getter pour login
    public function getLogin() : string {
        return $this->login;
    }

    // Setter pour login limité à 64 caractères
    public function setLogin(string $login) : void {
        $this->login = substr($login, 0, 64);
    }

    // Constructeur
    public function __construct(string $login, string $nom, string $prenom) {
        $this->login = substr($login, 0, 64); // Limitation à 64 caractères
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Méthode __toString()
    public function __toString() : string {
        return "ModeleUtilisateur: {$this->prenom} {$this->nom}, login: {$this->login}";
    }
}
?>