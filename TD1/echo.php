<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          //test
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;

          $nom = "Perchet";
        $prenom= "Aubin";
        $login= "percheta";
        echo "<br><br><br><br>";
        echo "<br>ModeleUtilisateur $prenom $nom de login $login<br>";

        $utilisateur = [
                "nom" => "Perchet",
                "prenom" => "Aubin",
                "login" => "percheta"
        ];

        $utilisateur['tel']= '0785623032';
        echo "<br> var_dump :";
        var_dump($utilisateur);
        echo "<br> print_r :";
        print_r($utilisateur);

        // Syntaxe avec {$...}
        echo "<br>Je m'appelle {$utilisateur['nom']}";
        echo "<br>Je m'appelle {$utilisateur["nom"]}";
        // Syntaxe simplifiée
        // Attention, pas de guillemets autour de la clé "nom"
        echo "<br>Je m'appelle  $utilisateur[nom]";

        foreach ($utilisateur as $cleee => $valeur){
            echo "$cleee : $valeur\n";
        }

        $utilisateurs = [
            ["nom" => "Leblanc", "prenom" => "Juste", "login" => "leblancj"],
            ["nom" => "Dupont", "prenom" => "Marie", "login" => "dupontm"],
            ["nom" => "Martin", "prenom" => "Pierre", "login" => "martinp"]
        ];

        echo "<br>";

        if (empty($utilisateur)) {
            echo "<p>Il n'y a aucun utilisateur.</p>";
        } else {
            echo "<h2>Liste des utilisateurs :</h2>";
            echo "<ul>";
            foreach ($utilisateurs as $utilisateur) {
                echo "<li>ModeleUtilisateur {$utilisateur['prenom']} {$utilisateur['nom']} de login {$utilisateur['login']}</li>";
            }
            echo "</ul>";
        }

        ?>
    </body>
</html> 