<?php
require_once 'Utilisateur.php';
try {
    // Test correct avec des strings
    $utilisateur = new Utilisateur("trèslongloginquidevraitêtretroncépourrespecterles64caractères", "Leblanc", "Juste");
    echo $utilisateur;

    // Test avec une erreur de type : passage d'un tableau vide au lieu d'une chaîne
    $utilisateur = new Utilisateur([], "Leblanc", "Juste");
} catch (TypeError $e) {
    // Affiche l'erreur TypeError avec les détails
    echo "Erreur de type : " . $e->getMessage();
}
?>