<?php
/**
// Vérifie si le tableau $_GET n'est pas vide
if (!empty($_GET)) {
    // Affiche le contenu du tableau $_GET pour débogage
    echo "<pre>";
    print_r($_GET);
    echo "</pre>";

    // Récupération des données du formulaire
    $login = $_GET['login'] ?? '';
    $nom = $_GET['nom'] ?? '';
    $prenom = $_GET['prenom'] ?? '';

    // Affiche les informations de l'utilisateur
    echo "<p>ModeleUtilisateur créé : $prenom $nom, login : $login</p>";
} else {
    echo "<p>Aucune donnée reçue.</p>";
}
 */


// Vérifie si le tableau $_POST n'est pas vide
if (!empty($_POST)) {
    // Affiche le contenu du tableau $_POST pour débogage
    echo "<pre>";
    print_r($_POST);
    echo "</pre>";

    // Récupération des données du formulaire
    $login = $_POST['login'] ?? '';
    $nom = $_POST['nom'] ?? '';
    $prenom = $_POST['prenom'] ?? '';

    // Affiche les informations de l'utilisateur
    echo "<p>ModeleUtilisateur créé : $prenom $nom, login : $login</p>";
} else {
    echo "<p>Aucune donnée reçue.</p>";
}

?>
