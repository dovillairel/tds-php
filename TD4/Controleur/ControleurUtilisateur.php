    <?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]);
    }

    public static function afficherFormulaireCreation(){
    self::afficherVue('utilisateur/formulaireCreation.php');
    }


    public static function afficherDetail() {
        // Récupération du login passé dans l'URL
        $login = isset($_GET['login']) ? $_GET['login'] : null;

        if ($login) {
            // Récupérer l'utilisateur par son login
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);

            if ($utilisateur) {
                // Si l'utilisateur existe, on affiche la vue détail
                self::afficherVue('utilisateur/detail.php', ['utilisateur' => $utilisateur]);
            } else {
                // Si l'utilisateur n'est pas trouvé, on affiche la vue d'erreur
                self::afficherVue('utilisateur/erreur.php');
            }
        } else {
            // Si aucun login n'est fourni, on affiche également la vue d'erreur
            self::afficherVue('utilisateur/erreur.php');
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }


}
?>
