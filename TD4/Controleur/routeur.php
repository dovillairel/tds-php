<?php
require_once 'ControleurUtilisateur.php';

$action = isset($_GET['action']) ? $_GET['action'] : null;

// Vérifier que l'action existe bien et que la méthode correspondante existe dans le contrôleur
if ($action && method_exists('ControleurUtilisateur', $action)) {
    // Appel de la méthode statique $action de ControleurUtilisateur
    ControleurUtilisateur::$action();
} else {
    // Si l'action n'est pas valide ou non spécifiée, on peut afficher une erreur ou rediriger
    echo "Action invalide ou non spécifiée.";
}

?>
