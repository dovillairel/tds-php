<?php
require_once 'Modele/ConnexionBaseDeDonnees.php';
require_once 'Modele/ModeleUtilisateur.php';

echo "<h3>Exercice 1</h3>";

$log1 = "doublets";
$log2 = "doublet";

$utilisateurLogin = ModeleUtilisateur::recupererUtilisateurParLogin($log1);
if ($utilisateurLogin !== null) {
    $utilisateurLogin->afficher();
} else {
    echo "Aucun utilisateur trouvé pour le login $log1.<br>";
}

echo "<br>";

$utilisateurLogin = ModeleUtilisateur::recupererUtilisateurParLogin($log2);
if ($utilisateurLogin !== null) {
    $utilisateurLogin->afficher();
} else {
    echo "Aucun utilisateur trouvé pour le login $log2.<br>";
}

echo "<h3>Exercice 2</h3><br>";

$utilisateur = new ModeleUtilisateur('iron.man','man','iron');
$utilisateur->afficher();

//$utilisateur->ajouter();

$utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
foreach ($utilisateurs as $utilisateur) {
    $utilisateur->afficher();
    echo "<br>";
}






