<?php

require_once 'Modele/ConnexionBaseDeDonnees.php';
require_once 'Modele/ModeleUtilisateur.php';

echo "<h3>Liste des utilisateurs <h4>1/2</h4></h3>";

$mhm = new ConnexionBaseDeDonnees();
$pdoStatement = $mhm->getPdo()->query("SELECT * FROM utilisateur");

foreach ($pdoStatement as $utilisateurFormatTableau) {

    $utilisateur = new ModeleUtilisateur(
        $utilisateurFormatTableau['loginBaseDeDonnees'],
        $utilisateurFormatTableau['prenomBaseDeDonnees'],
        $utilisateurFormatTableau['nomBaseDeDonnees']
    );
    echo "$utilisateur <br>";
}

echo "<br><h3>Liste des utilisateurs <h4>2/2</h4></h3><br>";

$utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
foreach ($utilisateurs as $utilisateur) {
    $utilisateur->afficher();
    echo "<br>";
}




