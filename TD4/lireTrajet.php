<?php

require_once 'Modele/ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';

echo "<h3>Liste des trajets :</h3>";

$trajets = Trajet::recupererTrajets();

foreach ($trajets as $trajet) {
    echo $trajet;
    echo "<h4>Passagers :</h4>";
    foreach ($trajet->getPassagers() as $passager) {
        echo $passager->getPrenom() . " " . $passager->getNom() . "<br>";
    }
}

?>
