<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{

    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void{

        if ($dureeExpiration == null || $dureeExpiration<=0){
            setcookie($cle,serialize($valeur));
        }
        else{
            setcookie($cle,serialize($valeur),time()+$dureeExpiration);
        }
    }

    public static function lire(string $cle) : mixed
    {
    return unserialize($_COOKIE[$cle]);
    }

    public static function contient($cle) : bool
    {
    return array_key_exists($cle, $_COOKIE);
    //isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void{

        unset($_COOKIE[$cle]);
    }



}