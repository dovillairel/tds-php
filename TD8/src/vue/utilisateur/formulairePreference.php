<?php
/** @var Utilisateur $utilisateur */
use App\Covoiturage\Modele\DataObject\Utilisateur;
?>

<form method="get" action="">
    <fieldset>
        <legend>Préférences</legend>
        <p class="InputAddOn">
            <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur">
            <label for="utilisateurId">Utilisateur</label>
        </p>
        <p class="InputAddOn">
            <input type="radio" id="trajetId" name="controleur_defaut" value="trajet">
            <label for="trajetId">Trajet</label>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
        <input type="hidden">
    </fieldset>
</form>