<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $titre; ?></title>
        <link rel="stylesheet" href="../ressources/style.css">
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li>
                        <a href="?action=afficherFormulairePreference"> <img src="../ressources/images/heart.png"> </a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                    </li><li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                    </li>
                </ul>
            </nav>
        </header>
        <main>
            <?php
                require __DIR__ . "/{$cheminCorpsVue}";
            ?>

            <a href="?action=afficherFormulaireCreation"> <img src="../ressources/images/add-user.png"> </a>
        </main>
        <footer>
            <p>
                Site de covoiturage de ...
            </p>
        </footer>
    </body>
</html>