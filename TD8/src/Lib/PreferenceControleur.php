<?php
class PreferenceControleur {
private static string $clePreference = "preferenceControleur";

public static function enregistrer(string $preference) : void
{
Cookie::enregistrer(PreferenceControleur::$clePreference, $preference);
}

public static function lire() : string
{
echo (PreferenceControleur::$clePreference);
return PreferenceControleur::$clePreference;
}

public static function existe() : bool
{
return isset($_COOKIE["preferenceControleur"]);
}

public static function supprimer() : void
{
    unset($_COOKIE["preferenceControleur"]);
}
}

