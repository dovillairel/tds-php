<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib as PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherErreur(string $messageErreur): void {
        self::afficherVue("vueGenerale.php", ["titre" => "error page", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }

    public static function afficherFormulairePreference(): void
    {
      self::afficherVue("vueGenerale.php", ["titre" => "Formulaire Preference", "cheminCorpsVue" => "utilisateur/formulairePreference.php"]);
    }

    public static function enregistrerPreference()
    {
        $controleur_defaut=$_GET["controleur_defaut"];
        PreferenceControleur::enregistrer($controleur_defaut);
        self::afficherVue("vueGenerale.php", ["titre" => "Préférence", "cheminCorpsVue" => "utilisateur/preferenceEnregistree.php", "preference" => $controleur_defaut]);
    }
}