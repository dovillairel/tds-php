<?php

namespace App\Covoiturage\Controleur;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherErreur(string $messageErreur): void {
        self::afficherVue("vueGenerale.php", ["titre" => "error page", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }

    protected static function afficherFormulairePreference(): void
    {
      self::afficherVue("vueGenerale.php", ["titre" => "Formulaire Preference", "cheminCorpsVue" => "/formulairePreference.php"]);
    }
}